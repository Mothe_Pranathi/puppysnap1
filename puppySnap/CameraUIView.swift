//
//  CameraUIView.swift
//  puppySnap
//
//  Created by Haider,Zachary M on 4/16/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class CameraUIView: UIView {
    
    let button = UIButton()
    
        button.frame = CGRect(x: self.view.frame.size.width - 60, y: 60, width: 50, height: 50)
    button.backgroundColor = UIColor.red
    button.setTitle("Name your Button ", for: .normal)
    button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    self.view.addSubview(button)
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
    

}
