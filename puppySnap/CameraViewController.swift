
//  CameraViewController.swift
//  puppySnap
//
//  Created by Haider,Zachary M on 3/2/19.
//  Copyright © 2019 Student. All rights reserved.
//  Bassed on "https://github.com/brianadvent/CustomCamera/blob/master/CustomCamera/ViewController.swift"

import UIKit
import AVFoundation // Library that includes all of the cammera functionality and frame manipulation
class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    let captureSession = AVCaptureSession()//this is the instance of the object that controlls the cammera data stream
    var previewLayer:CALayer! //this is a CAlayer Object that will replace the frame object that the view control has by defualt
    var captureDevice: AVCaptureDevice! // this will set wich built in cammera we want to view from
    var hasTakenPhoto = false
    var cameraSession = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(!cameraSession){
            prepCamera()
        }
    }
    
    @IBAction func playSound(_ sender: Any) {
        PlaySound.player.playSound()
    }
    
    @objc func buttonAction(sender: UIButton!) {
        self.hasTakenPhoto = true
    }
    
    //this function adds the custom shutter button to the uiview above the calayer and links it to the buttonAction function
    func makeButton(){
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.view.layer.addSublayer(self.previewLayer)
        let button = UIButton()
        button.setImage(UIImage(named: "shutterbutton.png"), for: UIControl.State.normal)
        button.frame = CGRect(x: self.view.frame.width - 25 - self.view.frame.width/2, y: self.view.frame.height - self.view.frame.height/7, width: 50, height: 50)
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    //prep camera will find all available camera input devices and pick one of the selfi camera options and then call the begin session fuction at line 35
    func prepCamera(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        if let availableDevices:[AVCaptureDevice]? = AVCaptureDevice.DiscoverySession(/* creates an array of all available camera devices available to capture *///
            deviceTypes: [.builtInWideAngleCamera],
            mediaType: AVMediaType.video,         //all cameras that can stream
            position: .front).devices             //selfi cam
        {if availableDevices!.count != 0{ // if the phone or pad has any camera input devices
            captureDevice = availableDevices!.first // pick the first one
            beginSession() // begin captureing set up and captureing
            }}
        cameraSession = true
    }
    
    //beginSession will check if the selected capture device can be found and will start the video stream to the frame object
    func beginSession(){
        do{
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        }
        catch{
            print(error.localizedDescription)
        }
        
        makeButton()
        self.previewLayer.frame = self.view.layer.frame
        captureSession.startRunning() //the stream of data coming from the camera is now set and active
        // lines 51 - 55 set the camera display setings to preset templets and displays the output on the set UIView Frame object
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [String(kCVPixelBufferPixelFormatTypeKey):kCVPixelFormatType_32BGRA]
        dataOutput.alwaysDiscardsLateVideoFrames = true
        
        if captureSession.canAddOutput(dataOutput){
            captureSession.addOutput(dataOutput)
        }
        captureSession.commitConfiguration()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: ""))
        
    }
    
    func getImageFromSampleBuffer(buffer:CMSampleBuffer)-> UIImage?{
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer){
            let ciImage = CIImage(cvPixelBuffer:pixelBuffer)
            let context = CIContext()
            let imageRect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))
            if let image = context.createCGImage(ciImage, from: imageRect){
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .right)
            }
        }
        return nil
    }
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if self.hasTakenPhoto{
            self.hasTakenPhoto  = false
            cameraSession = false
            if let image = self.getImageFromSampleBuffer(buffer: sampleBuffer){
                let photoVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"PhotoVC") as! PhotoViewController
                photoVC.photo = image
                DispatchQueue.main.async {
                    self.present(photoVC,animated: true,completion:{ self.stopCapture()})
                }
            }
        }
    }
    
    func stopCapture(){
        self.captureSession.stopRunning()
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput]{
            for input in inputs{
                self.captureSession.removeInput(input)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
}
