//
//  SoundViewController.swift
//  puppySnap
//  Created by Pranathi Mothe on 2/25/19.
//  Copyright  2019 Student. All rights reserved.
//
import UIKit
import AVFoundation

class SoundViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var soundsTV: UITableView!
    //An instance variable is created to hold the AVAudioplayer object
    var audioPlayer =  AVAudioPlayer()
    
    var settings = ["rattle-toy", "squeeky-toy","cat-meow-ii","chihuahua-puppy-whine"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    //The function is used to retreive the setting details
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sounds", for: indexPath)
        cell.textLabel?.text = settings[indexPath.row]
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        soundsTV.delegate = self
        soundsTV.dataSource = self
        soundsTV.allowsMultipleSelection = false
    }
    //notifies the view controller that its view is about to be added and stops the sound when it will appear in this tab.
    override func viewWillAppear(_ animated: Bool) {
        PlaySound.player.stopSound()
    }
    //This function allows the cell to be selected.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath)?.accessoryType == UITableViewCell.AccessoryType.checkmark{
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.none
             PlaySound.player.setSound(soundSelected: self.settings[indexPath.row])
        }
        else{
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
            PlaySound.player.setSound(soundSelected: self.settings[indexPath.row])
        }
        
    }
    //This function ensure that deselect is called on all other cells when a cell is selected.
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.none
    }
}
