//
//  PhotoViewController.swift
//  puppySnap
//
//  Created by Haider,Zachary M on 3/10/19. modified by Danel Favor
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: UIViewController {
    
    var photo:UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func goBackBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (photo != nil){
            imageView.image = photo
        }
    }
    @IBAction func saveToLibrary(_ sender: AnyObject) {
        CustomPhotoAlbum.sharedInstance.saveImage(image: imageView.image!)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    // This class was based off This forum "https://stackoverflow.com/questions/28708846/how-to-save-image-to-custom-album"
    
    class CustomPhotoAlbum {
        
        static let albumName = "PuppySnap"
        static let sharedInstance = CustomPhotoAlbum()
        
        var assetCollection: PHAssetCollection!
        
        init() {
            
            func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
                
                let fetchOptions = PHFetchOptions()
                fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
                let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
                
                if let _: AnyObject = collection.firstObject {
                    return collection.firstObject
                }
                
                return nil
            }
            
            if let assetCollection = fetchAssetCollectionForAlbum() {
                self.assetCollection = assetCollection
                return
            }
            
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)
            }) { success, _ in
                if success {
                    self.assetCollection = fetchAssetCollectionForAlbum()
                }
            }
        }
        
        func saveImage(image: UIImage) {
            
            if assetCollection == nil {
                return   // If there was an error upstream, skip the save.
            }
            
            PHPhotoLibrary.shared().performChanges({
                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                albumChangeRequest!.addAssets([assetPlaceholder] as NSFastEnumeration)
            }, completionHandler: nil)
        }
        
        
    }
    

}
