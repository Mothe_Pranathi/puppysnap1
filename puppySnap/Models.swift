//
//  Models.swift
//  puppySnap
//
//  Created by Pranathi Mothe on 3/29/19.
//  Copyright © 2019 Pranathi Mothe All rights reserved.
//

import Foundation
import AVFoundation //This is a framework in which we can play sounds

class PlaySound {
    //An instance variable is created to hold the AVAudioplayer object
    var audioPlayer =  AVAudioPlayer()
    static var player = PlaySound()
    
    private init() {
        //created an new AVAudioplayer object and intialize it with the path to the sound files.
        //Bundle is used to find our way inside the app to go find the sound files which are selected.
        let sound = Bundle.main.path(forResource: "squeeky-toy", ofType: "wav")
        do{
            //We try to get initialize it with the URL we created above.
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
        }
        catch{
            //It will print the error.
            print(error)
        }
    }
    //This function is set the sound file which is selected.
    func setSound(soundSelected:String){
        let sound = Bundle.main.path(forResource: soundSelected, ofType: "wav")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
        }
        catch{
            print(error)
        }
    }
    //It will play the sound.
    func playSound(){
        audioPlayer.play()
    }
    //It will stop the sound.
    func stopSound(){
        audioPlayer.stop()
    }
}
