//
//  SettingsViewController.swift
//  puppySnap
//This app was developed by: puppysnap
//  Created by Subodh Bhargav  on 3/11/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var settings = ["About","Sound Selection"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath)
            cell.textLabel?.text = "About"
            return cell
        }

        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "soundCell", for: indexPath)
            cell.textLabel?.text = "Sound selection"
            return cell
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
